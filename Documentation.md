# Google Native 

## Integration Steps

1) **"Install"** or **"Upload"** FGGoogleNative plugin from the FunGames Integration Manager in Unity, or download it from here.

2) Fyou will find the last compatible version of GoogleMobileAds-native package in the _Assets > FunGames_Externals > InGameAds_ folder. Double click on the .unitypackage file to install it.

3) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

4) In Assets/FunGames/Monetization/Ads/InGameAds/GoogleNative, you will find templates examples for 3D and 2D placements. Add placements in your scene and set it according to your app specifications.

> **Note :** To test your integration, you can enable “Test Mode” in FGGoogleUMPSettings, and add your device ID following step 2 of the following [link](https://developers.google.com/admob/unity/test-ads#add_your_test_device_programmatically).

## Backfill feature

To integrate Google Native placement to our backfill feature, just add the corresponding placement to the FGIGAdPlacementWithRemoteConfig billboard in the “GoolgeNative” placement field. (Remember you can also put your placement inside a parent and drag the parent insteads).

Remote Config value to be used in the **InGameAdsConfig** parameter is : **4** 